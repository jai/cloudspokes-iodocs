Issues
=========

Listed below are the issues I've found with the existing cloudspokes API while working on this project. I would suggest to fix these issues with the website in order to have more consistent API.

https://github.com/cloudspokes/cs-website has been taken as reference here.


1. `/leaderboards` api: json output only provides `@this_month_leaders` while the html output gives it for month, year and all time sections as well. Because of this `page_month` and `page_year` params have not been added in the IODocs params list. This looks inconsistent from the API name.

2. At some places, the API requires page numbers as parameters, while at other places it doesn't. This provides an inconsistent experience. e.g. `/leaderboard_this_year` provides an attribute for `per_page` whic his not there for `/leaderboard_this_month`

3. Attribute name in members section is `order_by` while in the challenges section, it is `orderby`

4. `/members` api doesn't need to fetch `@leaderboards` if the request format is `json` as it doesn't reutrn leaderboard in json response.

5. `/members` api can accept more parameters for sorting than expected (`total_wins__c`, `challenges_entered__c`, `name`).  These three are the only parameters which are being using for sorting on the main website. But other params of the table can also be used sort the result via API.

7. Similar to point 4, `members/search` does not need to fetch leaderboard if the response type is `json`

8. for all the APIs error conditions should be handled with proper response code in the same format. Currently errors responses are given back in html.

9. for `participant_scorecard` method, in `challenges_controller`, `json` is being generated but, but it is not getting returned in response. Should be a one line change to make this method compatible with API.

10. `members/past_challenges` and `members/active_challenges` methods receive usernames in `id` params. It should be changed to `username`

11. `active_challenges` method of members controller seems to be buggy at the moment. While `past_challenges` gives the result as expected, `active_challenges` doesn't provide correct result.

