Implementing I/O Docs for Cloudspokes API
=========================================
I/O Docs is a live interactive documentation system for RESTful web APIs. By defining APIs at the resource, method and parameter
levels in a JSON schema, I/O Docs will generate a JavaScript client interface. API calls can be executed from this interface, which are then proxied through the I/O Docs server with payload data cleanly formatted (pretty-printed if JSON or XML).

Build/Runtime dependencies
--------------------------
1. Node.js - server-side JS engine
2. npm - node package manager
3. Redis - key+value storage engine

Note: Node and some of the modules require compiler (like gcc). If you are on a Mac, you will need to install XCode. If you're on Linux, you'll need to install build-essentials, or something equivalent.

Installation instructions - Node, NPM & Redis
-----------------------------------------------
1. Node.js - [https://github.com/joyent/node/wiki/Installation](https://github.com/joyent/node/wiki/Installation)
2. npm (Node package manager) - [https://github.com/isaacs/npm](https://github.com/isaacs/npm)
3. Redis - [http://redis.io/download](http://redis.io/download)

Installation Instructions - I/O Docs for Cloudspokes
----------------------------------------
From the command line type in:
<pre>  git clone https://jai@bitbucket.org/jai/cloudspokes-iodocs.git
  cd cloudspokes-iodocs
  npm install
</pre>


### Node Module Dependencies
These will be automatically installed when you use any of the above *npm* installation methods above.

1. [express](http://expressjs.com/) - framework
2. [oauth](https://github.com/ciaranj/node-oauth) - oauth library
3. [redis](https://github.com/mranney/node_redis) - connector to Redis
3. [connect-redis](https://github.com/visionmedia/connect-redis) - Redis session store
4. [querystring](https://github.com/visionmedia/node-querystring) - used to parse query string
5. [jade](http://jade-lang.com/) - the view engine

### Running I/O Docs

1. Open terminal and change directory to cloudspokes-iodocs root
2. node ./app.js
3. Point your browser to: [http://localhost:3000](http://localhost:3000)

Screenshot
----------
<img src='public/images/cloudspokes-iodocs.png' alt='screenshot' width='600' />

How to add new api methods to I/O docs
--------------------------------------

1. If a new auth system is added to the API, it can be edited in `public/data/apiconfig.json`

	Current snapshot of this file looks something like this: 

		"cloudspokes": {
        	"name": "CloudSpokes API",
        	"protocol": "http",
        	"baseURL": "www.cloudspokes.com",
        	"publicPath": "",
        	"auth": ""
    	}

2. Editing an existing method

	`public/data/cloudspokes.json` contains method declarations
			
	   {
        "name": "Content",
        "methods": [
        	{
            	"MethodName": "notifications",
            	"Synopsis": "Returns notifications on cloudspokes website",
            	"HTTPMethod": "GET",
            	"URI": "/notifications.json",
            	"RequiresOAuth": "N",
            	"parameters": [
              		{
              		}
            	]
        	},
        	{
            	"MethodName": "home",
            	"Synopsis": "Returns content for homepage",
            	"HTTPMethod": "GET",
            	"URI": "/home.json",
            	"RequiresOAuth": "N",
            	"parameters": [
              		{
              		}
            	]
        	}
        ]
       }
       
	First we describe the resources. Each resource contains an array of methods. Each method is described as an object containing parameters such as `MethodName, Synopsis, HTTPMethod, URI, RequiresOAuth, parameters`. `parameters` is an array of parameters which the API method expects.

	The official documentation of IODocs explains it very well. It is available at: [https://github.com/mashery/iodocs]()



